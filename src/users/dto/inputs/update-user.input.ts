/* eslint-disable prettier/prettier */
import { Field, InputType } from '@nestjs/graphql';
import * as mongoose from 'mongoose';
import { CandidatureStatus } from 'src/common/types/candidatureStatus';
import { Competencies } from 'src/competencies/models/competence';
import { Sector } from 'src/sectors/models/sector';
import { Softskill } from 'src/softskills/models/softskills';

export type UserDocument = UpdateUserInput & mongoose.Document;

@InputType()
export class PreferencesInput {

  @Field(() => String, {nullable: true})
  language: String;

} 

@InputType()
export class CandidaturesInput {

    @Field(() => String) 
    offerID: String;

    @Field(() => CandidatureStatus , { nullable: true })
    status: CandidatureStatus;

    @Field(() => String) 
    offerTitle: String;
}

@InputType({ description: 'Update User document' })
export class UpdateUserInput {
    @Field({ nullable: true })
    name: string;
  
    @Field({ nullable: true })
    surname: string;
  
    @Field({ nullable: true })
    city: string;
    
    @Field({ nullable: true })
    website: string;
    
    @Field(() => [Sector], { nullable: true })
    sector: Sector[];

    @Field({ nullable: true })
    eduLevel: string;
  
    @Field({ nullable: true })
    email: string;

    @Field({ nullable: true })
    jobPosition: string;
      
    @Field({ nullable: true })
    lastJobTasks: string;

    @Field({ nullable: true })
    experience: string;

    @Field(() => [String], { nullable: true })
    languages: string[];

    @Field(() => [Competencies], { nullable: true })
    competencies: Competencies[];

    @Field(() => [Softskill], { nullable: true })
    softSkills: Softskill[];

    @Field(() => PreferencesInput, {nullable: true} )
    preferences: PreferencesInput;

    @Field({ nullable: true })
    video: string;

    @Field({nullable: true})
    avatarB64: string;

    @Field({nullable: true})
    coverLetter: string;

    // This two fields are updated diferently since they are synchronized with s3 storage
    cv: string;
    
    @Field(() => [CandidaturesInput], { nullable: 'itemsAndList'})
    candidatures: CandidaturesInput[]




 /*  
  @Field(() => [String])
  
  knowledge: string[];
*/
}
