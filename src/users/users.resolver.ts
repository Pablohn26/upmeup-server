/* eslint-disable prettier/prettier */
import { Headers, HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';
import { AuthGuard } from 'src/auth/auth.guard';
import { StorageService } from 'src/common/modules/s3storage/sotrageService';
import { S3Url } from 'src/common/types/s3Url';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserInput } from './dto/inputs/update-user.input';
import { userJWT } from './dto/user-jwt.dto';
import { User } from './models/user';
import { UsersService } from './users.service';


@Resolver(() => User)
export class UsersResolver {
  constructor(private readonly uService: UsersService, private readonly storageService: StorageService) {}

  //Get logged user Info
  @UseGuards(AuthGuard)
  @Query(returns => User, { name: 'me'})
  async getLoggedUser(@Context() context) {
    return this.uService.getUserById(context.user._id);
  }

  // User List to Logged
  // @UseGuards(AuthGuard)
  @Query((returns) => [User])
  async getUsersData(): Promise<User[]> {
    return this.uService.getUsersList();
  }

  // User By ID
  @Query((returns) => User)
  @UseGuards(AuthGuard)
  async getUser(@Args('id') id: string) {
    return await this.uService.getUserById(id);
  }


  // Get user auth JWT with email and password
  @Query((returns) => userJWT)
  async getJWTByEmailAndPassword(@Args('email') email: string, @Args('password') password: string) {
    return await { token: this.uService.createToken(email, password) }
  }

  @Query((returns) => S3Url, { name: 'downloadCvUrl'})
  @UseGuards(AuthGuard)
  async getCVPresignedUrl(@Context() context, @Args({ name: 'id', nullable: true }) id: string) {
    const user = await this.uService.getUserById(id ? id : context.user._id);
    return { presignedUrl: await this.storageService.getPresignedDownloadUrl(user._id.toString(), user.cv) };
  }

  @Query((returns) => S3Url, { name: 'downloadCoverLetterUrl'})
  @UseGuards(AuthGuard)
  async getCoverLetterPresignedUrl(@Context() context, @Args({name: 'id', nullable: true}) id: string) {
    const user = await this.uService.getUserById(id ? id : context.user._id);
    return { presignedUrl: await this.storageService.getPresignedDownloadUrl(user._id.toString(), user.coverLetter) };
  }

  @UseGuards(AuthGuard)
  @Query(() => S3Url, { name: 'uploadUrl'})
  async getPresignedUploadUrl(@Context() context, @Args('filename') filename: string) {
    return { presignedUrl: this.storageService.getPresignedUploadUrl(context.user._id, filename)}
  }

  /**
   * Mutation: Create new User resolver
  */
  @Mutation(() => User)
  async createUser(@Args('createUserDto') createUserDto: CreateUserDto): Promise<User> {
     return this.uService.createUser(createUserDto);
  }

  @UseGuards(AuthGuard)
  @Mutation(() => User, { name: 'addCandidature' })
  async addCandidature(
    @Args('userId') userId: string,
    @Args('offerID') offerID: string,
    @Args('offerTitle') offerTitle: string,
    @Context() context
  ) {
    return await this.uService.addCandidature(context.user._id, offerID, offerTitle);
  }

  @Mutation(() => User, {name: 'checkEmail'})
  async checkEmail(@Args('email') email: string){
    return this.uService.checkEmail(email);
  }
  

  /**
   * Mutation: Update User
   */
  @UseGuards(AuthGuard)
  @Mutation(() => User, { name: 'updateUser' })
  async updateUser(
    @Args('id') _id: string,
    @Args({ name: 'userInputs', type: () => UpdateUserInput }) input: UpdateUserInput,
    @Context() context
  ) {
    return await this.uService.updateUser(context.user._id, input);
  }

  @UseGuards(AuthGuard)
  @Mutation(() => User, { name: 'cv'})
  async updateCv(@Context() context, @Args({ name: 'filename', nullable: true}) filename: string) {
    return this.updateFileField(context, 'cv', filename);
  }

  @UseGuards(AuthGuard)
  @Mutation(() => User, { name: 'coverLetter'})
  async updateCoverLetter(@Context() context, @Args({ name: 'filename', nullable: true}) filename: string) {
    return this.updateFileField(context, 'coverLetter', filename);
  }

  @Mutation(() => Boolean, { name: 'resetToken'})
  async generatePasswordResetToken( @Args({ name: 'email' }) email: string) {
    return this.uService.generatePasswordResetToken(email);
  }

  @Mutation(() => Boolean, { name: 'resetPassword' })
  async resetPassword(@Args({ name: 'token'}) token: string, @Args({ name: 'password' }) password: string){
    return this.uService.resetPassword(token, password);
  }

  private async updateFileField(context, field: string, filename: string) {
    const updateUser = new UpdateUserInput();
    if(filename) {
      updateUser[field] = filename;
      if (await this.storageService.checkObjectExists(context.user._id, updateUser[field])) {
        const user = await this.uService.getUserById(context.user._id)
        if (user[field] !== updateUser[field]) {
          if (user[field])
            this.storageService.removeObject(context.user._id, user[field]);
          return await this.uService.updateUser(context.user._id, updateUser);
        }
        return user;
      }
      throw new HttpException('Provided file not found in storage', HttpStatus.BAD_REQUEST);
    } else {
      const user = await this.uService.getUserById(context.user._id)
      this.storageService.removeObject(context.user._id, user[field]);
      updateUser[field] = null;
      return this.uService.updateUser(context.user._id, updateUser);
    }
  }


}
