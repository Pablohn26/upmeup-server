import { BadRequestException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { compareSync, hashSync } from 'bcrypt';
import { randomBytes } from 'crypto';
import { sign } from 'jsonwebtoken';
import { Model } from 'mongoose';
import * as nodemailer from 'nodemailer';
import * as nunjucks from 'nunjucks';
import { CLIENT_URL, SIGNUP_MAIL_ENABLED } from '../constants';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserInput } from './dto/inputs/update-user.input';
import { User, UserDocument } from './models/user';


const HASH_ROUNDS = 10;

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private uModel: Model<UserDocument>, 
                                      private configService: ConfigService, 
                                      ) { }

  /**
   * Get User By User Logged ID
   * @param _id
   * @returns
  */
  async getUsersList(): Promise<User[]> {
    try {
      return this.uModel.find();
    } catch (error) {
      throw new BadRequestException();
    }
  }

  /**
   * Get User By UserID
   * @param _id
   * @returns
   */
  async getUserById(_id: string): Promise<User> {
    const user_found = this.uModel.findById(_id);
    console.log(`getUserById [${_id}] : ${user_found}`) // debug
    return user_found
  }

  async getUserByIdAndSearch(_id: string, search: string): Promise<any> {
    return this.uModel.findOne({ $or: [{ $text: { $search: search } }, { _id: _id }] }, { score: { $meta: "textScore" } });
  }

  /**
  * Get User By email
  * @param email
  * @returns User
  */
  async getUserByEmail(email: string): Promise<User> {
    console.log(`getUserByEmail : ${email}`) // debug
    return this.uModel.findOne({ email });
  }

  /**
   * Create new User
   **/
  async createUser(createUserDto: CreateUserDto): Promise<User> {
    
    // Encrypt password
    createUserDto.password = hashSync(createUserDto.password, HASH_ROUNDS)
    
    console.debug(`Creating new user [${createUserDto.email}]`) // TODO - Winston
    // console.debug(`Request info: ${JSON.stringify(createUserDto)}`) // debug
    
    // Check if a user with this mail already exists
    const existingUser = await this.uModel.find({ email: createUserDto.email });
    if (existingUser.length > 0) {
      console.log(`Returning Error ${HttpStatus.BAD_REQUEST} : BAD_REQUEST_EMAIL_EXISTS : ${createUserDto.email}`) // TODO - Winston
      throw new HttpException('BAD_REQUEST_EMAIL_EXISTS', HttpStatus.BAD_REQUEST);
    }
    const createdUser = new this.uModel(createUserDto);

    const result = createdUser.save() as any; // Saved into database
    console.log(`New user/company [${createdUser.email}] created:\n${JSON.stringify(createdUser)}`)  // TODO - Winston
    
    if (SIGNUP_MAIL_ENABLED) {
      this.sendSignupMail(createdUser)
    }
    else {
        console.log(`SIGNUP_MAIL_ENABLED = ${SIGNUP_MAIL_ENABLED} ; not sending any mail`);
    }
    
    // Async - do not care about result of mail sending
    return result
    
  }

  async addCandidature(userId: string, offerID: string, offerTitle: string) {

    const user = await this.getUserById(userId);

    const candidatures = user.candidatures as Array<any>;

    const userInput = new UpdateUserInput();

    candidatures.push({ offerID: offerID, offerTitle: offerTitle });


    userInput.candidatures = candidatures;
    return await this.uModel.findByIdAndUpdate(userId, userInput, { new: true });
  }

  async checkEmail(email: string) {
    
    // Check if a user with this mail already exists
    var existingUser = await this.uModel.find({ email: email });
    if (existingUser.length > 0) {
      // console.log(`Returning Error ${HttpStatus.BAD_REQUEST} : BAD_REQUEST_EMAIL_EXISTS : ${email}`) // TODO - Winston
      throw new HttpException('BAD_REQUEST_EMAIL_EXISTS', HttpStatus.BAD_REQUEST);
    }    
    return email;
    
  }



  async createToken(email: string, password: string) {
    console.log(`Creating tken for ${email}`) // debug
    const user = await this.uModel.findOne({ email });
    if (user && compareSync(password, user.password)) {
      console.log(`Token created OK for ${email}`) // debug
      return sign({ email: user.email, name: user.name, surname: user.surname, _id: user._id }, this.configService.get('JWT_SECRET'));
    }
    console.log(`Wrong password : ${email}`) // debug
    return ''
  }

  avatarB64resized: string;
  /**
   * Update User Info (except skills).
   * @param _id
   * @param updateUserInput
   * @returns
   */


  async updateUser(_id: string, updateUserInput: UpdateUserInput) {
    
    console.log(`Updating user [${_id}] with:\n${JSON.stringify(updateUserInput)}`) // TODO - Winston
    if (updateUserInput.avatarB64) {
      await this.processAvatar(updateUserInput);
      updateUserInput.avatarB64 = this.avatarB64resized;
    }
    return this.uModel.findByIdAndUpdate(_id, updateUserInput, { new: true });
  }

  async processAvatar(updateUserInput: UpdateUserInput) {
    let base64Parts = updateUserInput.avatarB64.split(';base64,');
    let base64Image = base64Parts.pop();
    let base64Header = base64Parts.pop();
    const imageBuffer = Buffer.from(base64Image, 'base64')
    const sharp = require('sharp');
    const avatarResized = await sharp(imageBuffer)
      .resize(110, 110)
      .toBuffer('Buffer')
    const avatarB64Resized = `${base64Header};base64,${avatarResized.toString('base64')}`
    this.avatarB64resized = avatarB64Resized
  }

  async generatePasswordResetToken(email: string): Promise<Boolean> {
    const user = await this.uModel.findOne({ email });
    if (!user) {
      throw new HttpException('USER_NOT_FOUND', HttpStatus.NOT_FOUND);
    }
    const lang = user.preferences?.language ? user.preferences.language : 'es';

    const token = randomBytes(16).toString('hex');
    let token_expiry = new Date();
    token_expiry.setDate(token_expiry.getDate() + 1);
    await this.uModel.findByIdAndUpdate(user._id, { token, token_expiry });
    const mailerTransport = nodemailer.createTransport({
      pool: true,
      host: this.configService.get('MAIL_SMTP_SERVER'),
      port: this.configService.get('MAIL_SMTP_PORT') ? this.configService.get('MAIL_SMTP_PORT') : 465,
      secure: true, // use TLS
      auth: {
        user: this.configService.get('MAIL_SMTP_USER'),
        pass: this.configService.get('MAIL_SMTP_PASSWORD'),
      },
    });

    nunjucks.configure({ autoescape: true });
    const message = {
      from: "hola@upmeup.es",
      to: user.email,
      subject: "Password Reset",
      text: nunjucks.render(`src/templates/password-reset/${lang}.txt`, { name: user.name, resetUrl: `${CLIENT_URL}/password-reset/${token}`}),
      html: nunjucks.render(`src/templates/password-reset/${lang}.html`, { name: user.name, resetUrl: `${CLIENT_URL}/password-reset/${token}`})

    };
    await mailerTransport.sendMail(message);
    return true;
  }

  async resetPassword(token: string, password: string) {
    const user = await this.uModel.findOne({ token });

    if (!user) throw new HttpException('TOKEN_NOT_FOUND', HttpStatus.NOT_FOUND);;
    if (user.token_expiry < new Date()) {
      await this.uModel.findByIdAndUpdate(user._id, { $unset: { token: '', token_expiry: '' } });
      throw new HttpException('TOKEN_EXPIRED', HttpStatus.AMBIGUOUS);
    }

    await this.uModel.findByIdAndUpdate(user._id, { password: hashSync(password, HASH_ROUNDS), $unset: { token: '', token_expiry: '' } });
    return true;
  }

  sendSignupMail(createdUser: User) {
    // const lang = createdUser.preferences?.language ? createdUser.preferences.language : 'es';
    const mailerTransport = nodemailer.createTransport({
      pool: true,
      host: this.configService.get('MAIL_SMTP_SERVER'),
      port: this.configService.get('MAIL_SMTP_PORT') ? this.configService.get('MAIL_SMTP_PORT') : 465,
      secure: true, // use TLS
      auth: {
        user: this.configService.get('MAIL_SMTP_USER'),
        pass: this.configService.get('MAIL_SMTP_PASSWORD'),
      },
    });

    nunjucks.configure({ autoescape: true });

    const userType = createdUser.type === '1' ? 'signup-confirmation' : 'signup-company-confirmation';

    const message = {
      from: "hola@upmeup.es",
      to: createdUser.email,
      subject: "Up me Up",
      // text: nunjucks.render(`src/templates/${userType}/${lang}.txt`),
      // html: nunjucks.render(`src/templates/${userType}/${lang}.html`)
      text: nunjucks.render(`src/templates/${userType}/es.txt`),
      html: nunjucks.render(`src/templates/${userType}/es.html`)
    };

    console.debug(`Sending signup mail to : [${createdUser.email}]`) // TODO - Winston
    mailerTransport.sendMail(message);

  }

  /*
  async findByLogin(UserDTO: LoginDTO) {
    const { email, password } = UserDTO;
    const user = await this.userModel.findOne({ email });
    if (!user) {
      throw new HttpException('user doesnt exists', HttpStatus.BAD_REQUEST);
    }
    if (await bcrypt.compare(password, user.password)) {
      return this.sanitizeUser(user);
    } else {
      throw new HttpException('invalid credential', HttpStatus.BAD_REQUEST);
    }
  }
  */
}
