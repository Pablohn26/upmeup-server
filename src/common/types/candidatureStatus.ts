export enum CandidatureStatus {
    UNDEFINED,
    HIRED,
    PRESELECTED,
    REJECTED
}