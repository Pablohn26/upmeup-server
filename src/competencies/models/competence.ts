/* eslint-disable prettier/prettier */
import { Field, ID, InputType, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types as MongooseTypes } from 'mongoose';

@Schema()
@ObjectType({ description: 'from CompetenceModel ' })
@InputType('EmbeddedCompetency')
export class Competencies {
  @Field(() => ID)
  _id: MongooseTypes.ObjectId;

  @Field(() => String)
  @Prop()
  name: string;
}
export type CompetencieDocument = Competencies & Document;
export const CompetenceSchema = SchemaFactory.createForClass(Competencies);