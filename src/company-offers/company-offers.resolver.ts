/* eslint-disable prettier/prettier */
import { Resolver, Query, Args, Mutation, Context } from '@nestjs/graphql';
import { Offer } from './models/company-offer';
import { CompanyOffersService } from './company-offers.service';
import { CreateOfferDto } from './dto/create-offer.dto';
import { UpdateOfferInput } from './dto/inputs/update-offer.input';
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/auth/auth.guard';
import { OfferFilter } from './dto/inputs/offer-filter.input';
import { CandidatureStatus } from 'src/common/types/candidatureStatus';
import { OfferStatus } from 'src/common/types/offerStatus';
import { S3Url } from 'src/common/types/s3Url';
import { StorageService } from 'src/common/modules/s3storage/sotrageService';
import { title } from 'process';

@Resolver(() => Offer)
export class CompanyOffersResolver {
  constructor(private readonly coffService: CompanyOffersService, private readonly storageService: StorageService) { }

  @UseGuards(AuthGuard)
  @Query((returns) => [Offer])
  async getCompanyOffers(
    @Args('filter', { nullable: true }) filter?: OfferFilter,
    @Args('limit', { nullable: true }) limit?: number,
    @Args('skip', { nullable: true }) skip?: number): Promise<Offer[]> {
    return await this.coffService.findAll(filter, limit, skip);
  }

  /**
   * Offer By ID
   */
  @UseGuards(AuthGuard)
  @Query((returns) => Offer)
  async getOffer(@Args('id') id: string) {
    return this.coffService.getOffer(id);
  }

  @UseGuards(AuthGuard)
  @Query((returns) => Offer)
  async getOfferByTitle(@Args('title') title: string) {
    return this.coffService.getOfferByTitle(title);
  }

  @UseGuards(AuthGuard)
  @Query((returns) => [Offer], { name: 'myCandidatures' })
  async getLoggedUserCandidatures(@Context() context) {
    return this.coffService.getOffersByCandidate(context.user._id);
  }

  /**
   * Mutation: Create new offer resolver
   */
  @UseGuards(AuthGuard)
  @Mutation(() => Offer)
  async createOffer(@Args('createOfferDto') createOfferDto: CreateOfferDto): Promise<Offer> {
    return this.coffService.createOffer(createOfferDto);
  }

  @UseGuards(AuthGuard)
  @Query(() => S3Url, { name: 'uploadOfferUrl'})
  async getPresignedUploadUrl(@Args('id') id: string, @Args('filename') filename: string) {
    return { presignedUrl: this.storageService.getPresignedUploadUrl(id, filename)}
  }

  @Query((returns) => S3Url, { name: 'downloadFile'})
  @UseGuards(AuthGuard)
  async getCVPresignedUrl(@Args('id') id: string, @Args('file') file: string) {
    return { presignedUrl: await this.storageService.getPresignedDownloadUrl(id, file) };
  }

   /**
   * Mutation: Delete offer
   */
   @UseGuards(AuthGuard)
   @Mutation(() => Offer, { name: 'deleteOffer' })
   async deleteOffer(@Args('id') _id: string): Promise<Offer> {
     return this.coffService.deleteOffer(_id);
   }

  /**
   * Mutation: Update Offer
   */
  @UseGuards(AuthGuard)
  @Mutation(() => Offer, { name: 'updateOffer' })
  async updateOffer(
    @Args('id') _id: string,
    @Args({ name: 'offerInputs', type: () => UpdateOfferInput }) input: UpdateOfferInput,
    @Context() context
  ) {
    if (await this.coffService.getOfferByIdAndOwnerId(_id, context.user._id))
      return await this.coffService.updateOffer(_id, input);
    else
      throw new HttpException('Missing permissions to update this offer', HttpStatus.FORBIDDEN)
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Offer, { name: 'addCandidate' })
  async addCandidate(
    @Args('offerId') offerId: string,
    @Args('candidateId') candidateId: string,
    @Context() context
  ) {
    return await this.coffService.addCandidate(offerId, context.user._id);
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Offer, { name: 'updateCandidateStatus' })
  async updateCandidateStatus(
    @Args('offerId') offerId: string,
    @Args('candidateId') candidateId: string,
    @Args({ name: 'status', type: () => CandidatureStatus }) status: CandidatureStatus,
    @Context() context
  ) {
    return await this.coffService.updateCandidateStatus(offerId, candidateId, status);
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Offer, { name: 'updateOfferStatus' })
  async updateOfferStatus(
    @Args('offerId') offerId: string,
    @Args({ name: 'status', type: () => OfferStatus }) status: OfferStatus,
    @Context() context
  ) {
    return await this.coffService.updateOfferStatus(offerId, status);
  }

  /*Document de otra colección: */
  /*@ResolveField()
  async user(
      @Parent() offer: OfferDocument,
      @Args('populate') populate: boolean,
  ) {
      if (populate)
          await offer
              .populate({path: 'user', model: User.name})
              .execPopulate();

      return offer.userId;
  }*/
}
