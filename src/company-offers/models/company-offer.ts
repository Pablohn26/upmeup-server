/* eslint-disable prettier/prettier */
import { Field, ID, ObjectType, registerEnumType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document, Types as MongooseTypes } from 'mongoose';
import { CandidatureStatus } from 'src/common/types/candidatureStatus';
import { OfferStatus } from 'src/common/types/offerStatus';
import { RequiredExperience } from 'src/common/types/requiredExpirience';
import { Competencies } from 'src/competencies/models/competence';
import { User } from 'src/users/models/user';

registerEnumType(RequiredExperience, { name: 'RequiredExperience' });
registerEnumType(CandidatureStatus, { name: 'CandidatureStatus'});
registerEnumType(OfferStatus, { name: 'OfferStatus'});

@Schema()
@ObjectType({ description: 'from CompanyOfferModel ' })
export class Offer {
  @Field(() => ID)
  _id: MongooseTypes.ObjectId;

  @Field(() => String)
  @Prop()
  userId: string;

  @Field(() => String)
  @Prop()
  title: string;

  @Field(() => String)
  @Prop()
  eduLevel: string;

  @Field(() => String)
  @Prop()
  city: string;

  @Field(() => String)
  @Prop()
  salaryRange: string;

  @Field(() => String)
  @Prop()
  remote: string;

  @Field(() => String)
  @Prop()
  contractType: string;

  @Field(() => String)
  @Prop()
  workHours: string;

  @Field(() => [Competencies])
  @Prop()
  competencies: Competencies[];

  @Field(() => [Candidate], { nullable: 'itemsAndList' })
  @Prop()
  candidates: Candidate[];

  @Field(() => Number)
  @Prop({})
  enrolled: number;

  @Field(() => String)
  @Prop()
  description: string;

  @Field(() => String)
  @Prop()
  requirements: string;

  @Field(() => Date)
  @Prop()
  createdDate: Date;

  @Field(() => RequiredExperience, { nullable: true })
  @Prop({ type: String, enum: RequiredExperience })
  requiredExperience: RequiredExperience;  

  @Field(() => OfferStatus, { nullable: true } )
  @Prop({ type: String, enum: OfferStatus })
  status: OfferStatus;


  // TODO: Get info from owner
  @Field(() => User)
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  user: User;

  @Field(() => Number)
  @Prop()
  maxMatchScore: Number;

  @Field(() => Number, { nullable: true })
  match: Number;

  @Field(() => Boolean)
  @Prop({ index: true})
  active: Boolean;

  @Field(() => [String], { nullable: true })
  @Prop()
  file: string[];

}

export type OfferDocument = Offer & Document;
export const OfferSchema = SchemaFactory.createForClass(Offer);

OfferSchema.index({ title: 'text', description: 'text', requirements: 'text'}, { default_language: 'spanish'})

/*OfferSchema.virtual('userName', {
  ref: 'User',
  localField: '_id',
  foreignField: 'user',
});*/


@ObjectType()
export class Match {

  @Prop()
  competencies: number;

  @Prop()
  softSkills: number;

  @Prop()
  content: number;

  @Prop()
  experience: number;

  @Field(() => Number, { nullable: true })
  total: number;
}

@ObjectType()
export class Candidate {

  @Field(() => User)
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  user: User;

  @Field(() => CandidatureStatus, { nullable: true } )
  @Prop({ type: String, enum: CandidatureStatus })
  status: CandidatureStatus;

  @Field(() => Match, { nullable: true })
  match: Match;
}
